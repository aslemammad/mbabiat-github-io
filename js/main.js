$( document ).ready(function() {

  $( ".cross" ).hide();
  $( ".vertical-menu" ).hide();
  $( ".hamburger" ).click(function() {
  $( ".vertical-menu" ).slideToggle( "slow", function() {
  $( ".hamburger" ).hide();
  $( ".cross" ).show();
  });
  });
  
  $( ".cross" ).click(function() {
  $( ".vertical-menu" ).slideToggle( "slow", function() {
  $( ".cross" ).hide();
  $( ".hamburger" ).show();
  });
  });
  
  });